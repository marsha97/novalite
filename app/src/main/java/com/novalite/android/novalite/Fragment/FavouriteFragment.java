package com.novalite.android.novalite.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.novalite.android.novalite.R;

import java.util.zip.Inflater;

/**
 * Created by kali on 7/7/17.
 */

public class FavouriteFragment extends Fragment {

    public static FavouriteFragment newInstance(){
        FavouriteFragment instance = new FavouriteFragment();

        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favoruite, container, false);
        return view;
    }
}
