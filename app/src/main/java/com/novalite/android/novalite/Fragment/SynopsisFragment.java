package com.novalite.android.novalite.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.novalite.android.novalite.Interface.NovaliteAPI;
import com.novalite.android.novalite.Model.Book;
import com.novalite.android.novalite.R;

import java.util.ArrayList;

/**
 * Created by kali on 7/21/17.
 */

public class SynopsisFragment extends Fragment {
    private String title, author, synopsis;
    private TextView titleText, authorText, synopsisText;

    public static SynopsisFragment newInstance(){
        SynopsisFragment instance = new SynopsisFragment();
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_synopsis, container,false);

        title = getArguments().getString("title");
        author = getArguments().getString("author");
        synopsis = getArguments().getString("synopsis");

        titleText = (TextView) view.findViewById(R.id.titleText);
        authorText = (TextView) view.findViewById(R.id.authorText);
        synopsisText = (TextView) view.findViewById(R.id.synopsisText);

        titleText.setText(title);
        authorText.setText(author);
        synopsisText.setText(synopsis);

        return view;
    }
}
