package com.novalite.android.novalite.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kali on 7/20/17.
 */

public class Book {
    @SerializedName("cover")
    private String cover;

    @SerializedName("title")
    private String title;

    @SerializedName("author")
    private String author;

    @SerializedName("synopsis")
    private String synopsis;

    @SerializedName("id")
    private int id;

    @SerializedName("bookId")
    private int bookId;

    public int getBookId()
    {
        return bookId;
    }

    public String getSynopsis()
    {
        return synopsis;
    }


    public int getId()
    {
        return id;
    }


    public String getCover()
    {
        return cover;
    }

    public String getTitle()
    {
        return title;
    }

    public String getAuthor()
    {
        return author;
    }

}
