package com.novalite.android.novalite.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.novalite.android.novalite.Client.novaliteClient;
import com.novalite.android.novalite.Interface.NovaliteAPI;
import com.novalite.android.novalite.Model.UserResponse;
import com.novalite.android.novalite.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActiviity extends AppCompatActivity implements View.OnClickListener{
    private EditText mUsernameEdit, mPassEdit, mRepassEdit, mEmailEdit, mDPNameEdit;
    private Button registerBtn;
    String username, password, rePass, email, dpName;
    private NovaliteAPI mAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_activiity);

        mUsernameEdit = (EditText) findViewById(R.id.editUser);
        mPassEdit = (EditText) findViewById(R.id.editPass);
        mRepassEdit = (EditText) findViewById(R.id.editPass2);
        mEmailEdit = (EditText) findViewById(R.id.editEmail);
        mDPNameEdit = (EditText) findViewById(R.id.editDPName);

        registerBtn = (Button) findViewById(R.id.btnRegister);

        registerBtn.setOnClickListener(RegisterActiviity.this);
        novaliteClient clientObj = new novaliteClient();
        mAPI = clientObj.getRetrofitClient().create(NovaliteAPI.class);

    }

    @Override
    public void onClick(View view) {
        username = mUsernameEdit.getText().toString();
        password = mPassEdit.getText().toString();
        rePass = mRepassEdit.getText().toString();
        email = mEmailEdit.getText().toString();
        dpName = mDPNameEdit.getText().toString();

        Call<UserResponse> call = mAPI.registerUser(username, password, rePass, email, dpName);

        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                String message  = response.body().getMessageFromServer();

                if (message.equals("success")){
                    Toast.makeText(RegisterActiviity.this, "Registration Complete", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegisterActiviity.this, LoginActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(RegisterActiviity.this, message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Toast.makeText(RegisterActiviity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
