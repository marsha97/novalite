package com.novalite.android.novalite.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.novalite.android.novalite.Adapter.HomeAdapter;
import com.novalite.android.novalite.Client.novaliteClient;
import com.novalite.android.novalite.Interface.NovaliteAPI;
import com.novalite.android.novalite.Model.Book;
import com.novalite.android.novalite.Model.BookResponse;
import com.novalite.android.novalite.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kali on 7/7/17.
 */

public class ThrillerFragment extends Fragment {

    private GridView mGridView;
    private NovaliteAPI mAPI;
    private ArrayList<Book> books;
    public static FragmentManager fm;

    public static ThrillerFragment newInstance(){
        ThrillerFragment instance = new ThrillerFragment();
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_genre_grid, container,false);

        mGridView = (GridView) view.findViewById(R.id.gridView1);
        books = new ArrayList<>();
        displayBooks("Thriller");

        return view;
    }

    private void displayBooks(String genre) {
        novaliteClient client = new novaliteClient();
        mAPI = client.getRetrofitClient().create(NovaliteAPI.class);

        Call<BookResponse> call = mAPI.getBooksByGenre(genre);
        call.enqueue(new Callback<BookResponse>() {
            @Override
            public void onResponse(Call<BookResponse> call, Response<BookResponse> response) {
                books = response.body().getBooks();

                if (books != null) {
                    mGridView.setAdapter(new HomeAdapter(getActivity().getApplicationContext(), books));

                    mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            int bookId = books.get(i).getId();
                            moveFragment(bookId);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<BookResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        displayBooks("Thriller");
    }

    private void moveFragment(int id) {
        Fragment fragment = DetailFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putInt("bookId", id);
        bundle.putString("fragment", "thriller");

        fragment.setArguments(bundle);

        fm = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
