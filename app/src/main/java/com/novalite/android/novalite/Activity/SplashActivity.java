package com.novalite.android.novalite.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.novalite.android.novalite.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.NoTitleBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread myThread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(3000);
                    Intent intent;
                    SharedPreferences statusPref = getSharedPreferences("UserInfo", 0);
                    String status = statusPref.getString("user_status", "loggedOut");

                    if (status.equals("loggedOut")){
                        intent = new Intent(getApplicationContext(), LoginActivity.class);
                    }
                    else {
                        intent = new Intent(getApplicationContext(), MainActivity.class);
                    }
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();
    }
}