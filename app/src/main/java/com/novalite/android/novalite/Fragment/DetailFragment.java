package com.novalite.android.novalite.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.novalite.android.novalite.Activity.MainActivity;
import com.novalite.android.novalite.Adapter.ChapterAdapter;
import com.novalite.android.novalite.Client.novaliteClient;
import com.novalite.android.novalite.Interface.NovaliteAPI;
import com.novalite.android.novalite.Model.Book;
import com.novalite.android.novalite.Model.BookResponse;
import com.novalite.android.novalite.Model.Chapter;
import com.novalite.android.novalite.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kali on 7/21/17.
 */

public class DetailFragment extends Fragment implements View.OnClickListener{
    private int bookId;
    private TextView titleText, authorText, synopsisText;
    private Button moreBtn, addBtn;
    private ImageView coverImage;
    private ArrayList<Chapter> chapterList;
    private String title, author, synopsis, prevFragment;
    private RecyclerView mRecyclerView;
    private int clickedId;

    private NovaliteAPI mAPI;

    public static DetailFragment newInstance(){
        DetailFragment fragment = new DetailFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        bookId = getArguments().getInt("bookId");
        prevFragment = getArguments().getString("fragment");

//        Toast.makeText(getContext(), "prevFrag: " + prevFragment, Toast.LENGTH_SHORT).show();

        View view = inflater.inflate(R.layout.fragment_book_detail, container,false);

        titleText = (TextView) view.findViewById(R.id.titleText);
        authorText = (TextView) view.findViewById(R.id.authorText);
        synopsisText = (TextView) view.findViewById(R.id.synopsisText);
        coverImage = (ImageView) view.findViewById(R.id.coverImage);
        moreBtn = (Button) view.findViewById(R.id.moreButton);
        addBtn = (Button) view.findViewById(R.id.addButton);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.chapterList);
        chapterList = new ArrayList<>();

        if (prevFragment.equals("profile")){
            addBtn.setVisibility(View.VISIBLE);
        }
        else {
            addBtn.setVisibility(View.GONE);
        }

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(new ChapterAdapter(getContext(), chapterList,prevFragment));


        moreBtn.setOnClickListener(DetailFragment.this);
        addBtn.setOnClickListener(DetailFragment.this);

        novaliteClient client = new novaliteClient();
        mAPI = client.getRetrofitClient().create(NovaliteAPI.class);

        displayDetail();

        displayChapters(prevFragment);

        return view;
    }

    private void displayChapters(final String fragment) {
        Log.i("DetailFragment display", fragment);

        Call<BookResponse> call = mAPI.getChapterList(bookId);
        call.enqueue(new Callback<BookResponse>() {
            @Override
            public void onResponse(Call<BookResponse> call, Response<BookResponse> response) {
                chapterList = response.body().getChapters();
//                Toast.makeText(getContext(), chapterList.get(1).getChapterTitle(), Toast.LENGTH_LONG).show();
                mRecyclerView.setAdapter(new ChapterAdapter(getActivity().getApplicationContext(), chapterList, fragment));
                mRecyclerView.smoothScrollToPosition(0);
            }

            @Override
            public void onFailure(Call<BookResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void displayDetail() {
        Call<Book> call = mAPI.getBookDetails(bookId);
        call.enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                title = response.body().getTitle();
                author = response.body().getAuthor();
                synopsis = response.body().getSynopsis();

                titleText.setText(title);
                authorText.setText(author);

                if (synopsis.length() <= 30){
                    synopsisText.setText(synopsis);
                    moreBtn.setVisibility(View.GONE);
                }
                else{
                    String reducedSynopsis = synopsis.substring(0, 29) + (".............");
                    synopsisText.setText(reducedSynopsis);

                    moreBtn.setVisibility(View.VISIBLE);
                }

                Picasso.with(getContext())
                        .load(response.body().getCover())
                        .placeholder(R.drawable.novalite)
                        .resize(150,200)
                        .into(coverImage);
            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {

            }
        });
    }

    public void setClickedId(int id)
    {
        clickedId = id;
    }

    public void moveToStoryFragment()
    {
       Log.i("DetailFragment", "Click: " + clickedId);
        Fragment storyFragment = StoryFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putInt("id", clickedId);
        storyFragment.setArguments(bundle);

        Log.i("DetailFragment", "prevFragment: " + prevFragment);

        FragmentManager fm = null;
        if (prevFragment.equals("action"))
        {
            fm = ActionFragment.fm;
        }
        else if (prevFragment.equals("fanfic"))
        {
            fm = FanFicFragment.fm;
        }
        else if (prevFragment.equals("fantasy"))
        {
            fm = FanFicFragment.fm;
        }
        else if (prevFragment.equals("horror"))
        {
            fm = HorrorFragment.fm;
        }
        else if (prevFragment.equals("comedy"))
        {
            fm = HumorFragment.fm;
        }
        else if(prevFragment.equals("nonfiction"))
        {
            fm = NonFictionFragment.fm;
        }
        else if(prevFragment.equals("romance"))
        {
            fm = RomanceFragment.fm;
        }
        else if(prevFragment.equals("shortStory"))
        {
            fm = ShortStoryFragment.fm;
        }
        else if(prevFragment.equals("spiritual"))
        {
            fm = SpiritualFragment.fm;
        }
        else if(prevFragment.equals("thriller"))
        {
            fm = ThrillerFragment.fm;
        }
        else if(prevFragment.equals("update"))
        {
            fm = UpdateFragment.fm;
        }
        else if(prevFragment.equals("profile"))
        {
            fm = ProfileFragment.fm;
        }
        if (fm != null) {
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragmentContainer, storyFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }


    @Override
    public void onClick(View view) {
        if (view == moreBtn)
        {
            moveToSynopsis();
        }
        else
        {
            moveToWrite();
        }
    }

    private void moveToWrite() {
        Fragment writeFragment = WriteFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putInt("bookId", bookId);
        writeFragment.setArguments(bundle);

        FragmentManager fm = ProfileFragment.fm;
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, writeFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    private void moveToSynopsis() {
        Fragment synopsisFrag = SynopsisFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("author", author);
        bundle.putString("synopsis", synopsis);
        synopsisFrag.setArguments(bundle);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fragmentContainer, synopsisFrag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void setPrefFrag(String fragment) {
        prevFragment = fragment;
        Log.i("DetailFragment", "set: " + fragment);
        moveToStoryFragment();
    }
}
