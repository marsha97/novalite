package com.novalite.android.novalite.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.novalite.android.novalite.Adapter.HomeAdapter;
import com.novalite.android.novalite.Client.novaliteClient;
import com.novalite.android.novalite.Interface.NovaliteAPI;
import com.novalite.android.novalite.Model.Book;
import com.novalite.android.novalite.Model.BookResponse;
import com.novalite.android.novalite.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kali on 7/7/17.
 */

public class ProfileFragment extends Fragment {

    private GridView mGridView;
    private NovaliteAPI mAPI;
    private ArrayList<Book> books;
    public static FragmentManager fm;
    private String author;

    public static ProfileFragment newInstance(){
        ProfileFragment instance = new ProfileFragment();
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_genre_grid, container,false);
        author = getArguments().getString("username");

        mGridView = (android.widget.GridView) view.findViewById(R.id.gridView1);
        books = new ArrayList<>();
        displayBooks(author);

        return view;
    }

    private void displayBooks(final String author) {
        novaliteClient client = new novaliteClient();
        mAPI = client.getRetrofitClient().create(NovaliteAPI.class);

        Call<BookResponse> call = mAPI.getBooksByAuthor(author);
        call.enqueue(new Callback<BookResponse>() {
            @Override
            public void onResponse(Call<BookResponse> call, Response<BookResponse> response) {
//                Toast.makeText(getContext(), new Gson().toJson(response.body()).toString(), Toast.LENGTH_LONG).show();
//                Toast.makeText(getContext(), author, Toast.LENGTH_LONG).show();
                books = response.body().getBooks();

                if (books != null) {
                    mGridView.setAdapter(new HomeAdapter(getActivity().getApplicationContext(), books));

                    mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            int bookId = books.get(i).getId();
                            moveFragment(bookId);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<BookResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        displayBooks(author);
    }

    private void moveFragment(int id) {
        Fragment fragment = DetailFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putInt("bookId", id);
        bundle.putString("fragment", "profile");

        fragment.setArguments(bundle);

        fm = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
