package com.novalite.android.novalite.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.novalite.android.novalite.R;

/**
 * Created by kali on 7/7/17.
 */

public class HomeFragment extends Fragment {
    private FragmentPagerAdapter mFragmentPagerAdapter;
    public static HomeFragment newInstance(){
        HomeFragment newIns =  new HomeFragment();
        return newIns;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container,false);
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        mFragmentPagerAdapter = new MyPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(mFragmentPagerAdapter);
        return view;
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter{
        final int TAB_COUNT = 10;
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0: return ActionFragment.newInstance();
                case 1: return FanFicFragment.newInstance();
                case 2: return FantasyFragment.newInstance();
                case 3: return HorrorFragment.newInstance();
                case 4: return HumorFragment.newInstance();
                case 5: return NonFictionFragment.newInstance();
                case 6: return RomanceFragment.newInstance();
                case 7: return ShortStoryFragment.newInstance();
                case 8: return SpiritualFragment.newInstance();
                case 9: return ThrillerFragment.newInstance();
                default: return null;
            }
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0: return "Action";
                case 1: return "Fanfiction";
                case 2: return "Fantasy";
                case 3: return "Horror";
                case 4: return "Comedy";
                case 5: return "Nonfiction";
                case 6: return "Romance";
                case 7: return "Short Story";
                case 8: return "Spiritual";
                case 9: return "Thriller";
                default: return "Invalid";
            }
        }
    }
}
