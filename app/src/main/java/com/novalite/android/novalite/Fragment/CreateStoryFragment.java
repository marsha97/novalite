package com.novalite.android.novalite.Fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.novalite.android.novalite.Activity.SingleFragment;
import com.novalite.android.novalite.Client.novaliteClient;
import com.novalite.android.novalite.Interface.NovaliteAPI;
import com.novalite.android.novalite.Model.BookResponse;
import com.novalite.android.novalite.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.id.list;
import static android.R.id.progress;
import static android.app.Activity.RESULT_OK;

/**
 * Created by kali on 7/7/17.
 */

public class CreateStoryFragment extends Fragment implements View.OnClickListener {
    private EditText titleEdit, genreEdit, synopsisEdit;
    private Button coverBtn, stroyBtn, genreBtn;
    private ImageView imageContainer;
    private List<CharSequence> genreList = new ArrayList<>();
    private Bitmap selectedImage;
    private String mediaPath;
    private String mSelectedImageUri;
    private String url;
    private NovaliteAPI mAPI;
    private ProgressDialog mProgressDialog;

    public static CreateStoryFragment newInstance(){
        CreateStoryFragment instance = new CreateStoryFragment();

        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create, container, false);

        titleEdit = (EditText) view.findViewById(R.id.editTitle);
        genreBtn= (Button) view.findViewById(R.id.btnGenre);
        synopsisEdit = (EditText) view.findViewById(R.id.editSynopsis);
        coverBtn = (Button) view.findViewById(R.id.pickBtn);
        stroyBtn = (Button) view.findViewById(R.id.writeBtn);
        imageContainer = (ImageView) view.findViewById(R.id.imagePick);
        mProgressDialog = new ProgressDialog(getContext());
        mediaPath = "";

        genreBtn.setOnClickListener(CreateStoryFragment.this);
        coverBtn.setOnClickListener(CreateStoryFragment.this);
        stroyBtn.setOnClickListener(CreateStoryFragment.this);

        genreList.add("Action");
        genreList.add("Fanfiction");
        genreList.add("Fantasy");
        genreList.add("Horror");
        genreList.add("Comedy");
        genreList.add("NonFiction");
        genreList.add("Romance");
        genreList.add("Short Story");
        genreList.add("Spiritual");
        genreList.add("Thriller");
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view == genreBtn){
            final CharSequence[] choices = genreList.toArray(new CharSequence[genreList.size()]);
            final AlertDialog.Builder builderDialog = new AlertDialog.Builder(getContext());
            builderDialog.setTitle("Choose Genre(s)");
            int count = choices.length;
            boolean[] isChecked = new boolean[count];

            builderDialog.setMultiChoiceItems(choices, isChecked, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i, boolean b) {

                }
            });

            builderDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    ListView listView = ((AlertDialog) dialogInterface).getListView();

                    // make selected item in the comma seprated string
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < listView.getCount(); i++) {
                        boolean checked = listView.isItemChecked(i);

                        if (checked) {
                            if (stringBuilder.length() > 0) stringBuilder.append(",");
                            stringBuilder.append(listView.getItemAtPosition(i));
                        }
                    }
                    if (stringBuilder.toString().trim().equals("")) {

                        genreBtn.setText("");
                        stringBuilder.setLength(0);

                    } else {
                        genreBtn.setText(stringBuilder);
                    }
                }
            });
            AlertDialog alert = builderDialog.create();
            alert.show();
        }
        else if (view == coverBtn){
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, 0);
        }
        else if (view == stroyBtn){
            novaliteClient client = new novaliteClient();
            url = client.getAPI_BASE_URL();
            mAPI = client.getRetrofitClient().create(NovaliteAPI.class);
            insertDatatoDB();
        }
    }

    private void insertDatatoDB() {
        RequestBody title = RequestBody.create(MediaType.parse("text/plain"),titleEdit.getText().toString());
        RequestBody genre = RequestBody.create(MediaType.parse("text/plain"),genreBtn.getText().toString());
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy");
        RequestBody date = RequestBody.create(MediaType.parse("text/plain"),dateFormat.format(calendar.getTime()));
        RequestBody synopsis = RequestBody.create(MediaType.parse("text/plain"),synopsisEdit.getText().toString());
        RequestBody author = RequestBody.create(MediaType.parse("text/plain"),getFromSharedPreferences());
        RequestBody coverUrl = RequestBody.create(MediaType.parse("text/plain"), url);
         //Map is used to multipart the file using okhttp3.RequestBody
        File file = new File(mediaPath);

        // Parsing any Media type file
        RequestBody mRequset = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("cover",file.getName(), mRequset);
        RequestBody fileName = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        Call<BookResponse> responseCall = mAPI.insertBook(title,genre,date,synopsis,author,coverUrl,fileToUpload, fileName);
        responseCall.enqueue(new Callback<BookResponse>() {
            @Override
            public void onResponse(Call<BookResponse> call, Response<BookResponse> response) {
                Log.i("DATABASE_LOG", "headers = " + response.headers().toString());
                String message = response.body().getMessage();
                int id = response.body().getId();
                mProgressDialog.setMessage("Uploading...");
                mProgressDialog.setTitle("Wait...");
                mProgressDialog.show();
                if(message.equals("success")){
                   mProgressDialog.hide();
                    moveToWrite(id);
                }
                else {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BookResponse> call, Throwable t) {
                Log.i("DATABASE_LOG", "t = " + t.getMessage());
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void uploadCover() {
//        // Map is used to multipart the file using okhttp3.RequestBody
//        File file = new File(mediaPath);
//
//        // Parsing any Media type file
//        RequestBody mRequset = RequestBody.create(MediaType.parse("*/*"), file);
//        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file",file.getName(), mRequset);
//        RequestBody fileName = RequestBody.create(MediaType.parse("text/plain"), file.getName());
//
//        Call<BookResponse> coverResponse = mAPI.insertCover(fileToUpload,fileName);
//
//        coverResponse.enqueue(new Callback<BookResponse>() {
//            @Override
//            public void onResponse(Call<BookResponse> call, Response<BookResponse> response) {
//                mProgressDialog.hide();
//                String message = response.body().getMessage();
//                if (message.equals("success")){
//                    moveToWrite();
//                }
//                else{
//                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<BookResponse> call, Throwable t) {
//                Log.i("DATABASE_LOG", "t= " + t.getMessage());
//            }
//        });}

    private void moveToWrite(int id) {
        Fragment writeFragment = WriteFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putInt("bookId", id);
        writeFragment.setArguments(bundle);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fragmentContainer, writeFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private String getFromSharedPreferences() {
        SharedPreferences preferences = getActivity().getSharedPreferences("UserInfo",0);
        return preferences.getString("username","anon");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        try{
            if (requestCode == 0 && resultCode == RESULT_OK && data != null){
                //get image from data
                Uri uri = data.getData();

                mSelectedImageUri = uri.toString();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContext().getContentResolver().query(uri, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                Toast.makeText(getContext(), mediaPath, Toast.LENGTH_SHORT).show();

                // Set the Image in ImageView for Previewing the Media
                selectedImage = BitmapFactory.decodeFile(mediaPath);
                imageContainer.setImageBitmap(selectedImage);
                cursor.close();

            }
            else {
                Toast.makeText(getContext(), "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e){
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

}
