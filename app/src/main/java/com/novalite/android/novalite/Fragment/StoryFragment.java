package com.novalite.android.novalite.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.novalite.android.novalite.Client.novaliteClient;
import com.novalite.android.novalite.Interface.NovaliteAPI;
import com.novalite.android.novalite.Model.Chapter;
import com.novalite.android.novalite.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kali on 7/21/17.
 */

public class StoryFragment extends Fragment {
    private TextView chapterText, chapterTitleText, storyText;
    private String chapterTitle, story;
    private int chapter;
    private int chapterId;
    private NovaliteAPI mAPI;

    public static StoryFragment newInstance()
    {
        StoryFragment fragment = new StoryFragment();
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_story, container,false);

        chapterId = getArguments().getInt("id");

        chapterText = (TextView) view.findViewById(R.id.chapterText);
        chapterTitleText = (TextView) view.findViewById(R.id.judulChapterText);
        storyText = (TextView) view.findViewById(R.id.storyText);

        novaliteClient client = new novaliteClient();
        mAPI = client.getRetrofitClient().create(NovaliteAPI.class);

        displayStory();

        return view;
    }

    private void displayStory() {
        Call<Chapter> call = mAPI.getStory(chapterId);
        call.enqueue(new Callback<Chapter>() {
            @Override
            public void onResponse(Call<Chapter> call, Response<Chapter> response) {
                chapter = response.body().getChapter();
                chapterTitle = response.body().getChapterTitle();
                story = response.body().getStory();

                chapterText.setText("Chapter " + chapter);
                chapterTitleText.setText(chapterTitle);
                storyText.setText(story);
            }

            @Override
            public void onFailure(Call<Chapter> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
