package com.novalite.android.novalite.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.novalite.android.novalite.Fragment.DetailFragment;
import com.novalite.android.novalite.Model.Chapter;
import com.novalite.android.novalite.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by kali on 7/21/17.
 */

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.CustomViewHolder> {
    private Context mContext;
    private ArrayList<Chapter> mChapters;
    private int id = 0;
    private final String TAG = "chapterList";
    private String prevFragment;

    public ChapterAdapter(Context context, ArrayList<Chapter> chapters, String fragment){
        this.mContext = context;
        this.mChapters = chapters;
        this.prevFragment = fragment;
    }

    @Override
    public ChapterAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_card, parent,false);
        return new  CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.chapterNTitle.setText("Chapter " + mChapters.get(position).getChapter() + " [" +
        mChapters.get(position).getChapterTitle()+"]");
        Log.i(TAG, mChapters.get(position).getChapterTitle());
        holder.date.setText(mChapters.get(position).getUpdate());
    }

    @Override
    public int getItemCount() {
        return mChapters.size();
    }

    public int getId()
    {
        return id;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public TextView chapterNTitle, date;

        public CustomViewHolder(View itemView) {
            super(itemView);

            chapterNTitle = (TextView) itemView.findViewById(R.id.chapterJudulText);
            date = (TextView) itemView.findViewById(R.id.tanggalText);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION)
                    {
                        id = mChapters.get(position).getChapterId();
                        DetailFragment detail = new DetailFragment();
                        detail.setClickedId(id);
                        detail.setPrefFrag(prevFragment);
                    }
                }
            });
        }
    }
}
