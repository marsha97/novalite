package com.novalite.android.novalite.Client;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kali on 7/6/17.
 */

public class novaliteClient {
    private String API_BASE_URL = "http://192.168.43.104/novalite/";

    public Retrofit getRetrofitClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder mBuilder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create());
        Retrofit mRetrofit = mBuilder.client(httpClient.build()).build();

        return mRetrofit;
    }

    public String getAPI_BASE_URL()
    {
        return API_BASE_URL;
    }
}
