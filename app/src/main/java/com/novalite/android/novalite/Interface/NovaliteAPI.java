package com.novalite.android.novalite.Interface;

import com.novalite.android.novalite.Model.Book;
import com.novalite.android.novalite.Model.BookResponse;
import com.novalite.android.novalite.Model.Chapter;
import com.novalite.android.novalite.Model.UserResponse;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by kali on 7/6/17.
 */

public interface NovaliteAPI {

    @Multipart
    @POST("insertBooks.php")
    Call<BookResponse> insertBook(@Part("title") RequestBody title, @Part("genre") RequestBody genre, @Part("date") RequestBody date, @Part("synopsis") RequestBody synopsis, @Part("author") RequestBody author,@Part("url") RequestBody url, @Part MultipartBody.Part cover, @Part("cover")RequestBody name);


    @FormUrlEncoded
    @POST("register.php")
    Call<UserResponse> registerUser(@Field("username") String username, @Field("password") String password, @Field("rePass") String
                      rePass, @Field("email") String email, @Field("displayName") String dpName);

    @FormUrlEncoded
    @POST("login.php")
    Call<UserResponse> loginUser(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("writeStory.php")
    Call<BookResponse> insertStory(@Field("bookId") int id, @Field("chapter") int chapter, @Field("chapterTitle") String chapterTitle, @Field("story") String story, @Field("date") String date);

    @FormUrlEncoded
    @POST("fetchBooks.php")
    Call<BookResponse> getBooksByGenre(@Field("genre") String genre);

    @FormUrlEncoded
    @POST("bookDetail.php")
    Call<Book> getBookDetails(@Field("bookId") int bookId);

    @FormUrlEncoded
    @POST("chapterList.php")
    Call<BookResponse> getChapterList(@Field("bookId") int bookId);

    @FormUrlEncoded
    @POST("bookStory.php")
    Call<Chapter> getStory(@Field("id") int id);

    @GET("getLatest.php")
    Call<BookResponse> getLatest();

    @FormUrlEncoded
    @POST ("authorBooks.php")
    Call<BookResponse> getBooksByAuthor(@Field("author") String author);
}
