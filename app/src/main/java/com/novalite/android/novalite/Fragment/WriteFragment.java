package com.novalite.android.novalite.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.novalite.android.novalite.Client.novaliteClient;
import com.novalite.android.novalite.Interface.NovaliteAPI;
import com.novalite.android.novalite.Model.BookResponse;
import com.novalite.android.novalite.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kali on 7/11/17.
 */

public class WriteFragment extends Fragment implements View.OnClickListener {
    private int id;
    private EditText chapterEdit, chapterTitleEdit, storyEdit;
    private Button uploadBtn;
    private NovaliteAPI mAPI;

    public static WriteFragment newInstance(){
        WriteFragment fragment = new WriteFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        id = getArguments().getInt("bookId");
//        Toast.makeText(getContext(), "book id: " + id, Toast.LENGTH_SHORT).show();

        View view = inflater.inflate(R.layout.fragment_write, container,false);

        chapterEdit = (EditText) view.findViewById(R.id.editChapter);
        chapterTitleEdit = (EditText) view.findViewById(R.id.editChapterTitle);
        storyEdit = (EditText) view.findViewById(R.id.editWrite);
        uploadBtn = (Button) view.findViewById(R.id.buttonSubmitStory);

        uploadBtn.setOnClickListener(WriteFragment.this);
        return view;
    }

    @Override
    public void onClick(View view) {
        novaliteClient mClient = new novaliteClient();
        mAPI = mClient.getRetrofitClient().create(NovaliteAPI.class);
        insertDataToDb();
    }

    private void insertDataToDb() {
        Integer chapter = Integer.parseInt(chapterEdit.getText().toString());
        String chapterTItle = chapterTitleEdit.getText().toString();
        String story = storyEdit.getText().toString();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy");
        String date  = dateFormat.format(calendar.getTime());

        Call<BookResponse> responseCall = mAPI.insertStory(id, chapter, chapterTItle, story, date);
        responseCall.enqueue(new Callback<BookResponse>() {
            @Override
            public void onResponse(Call<BookResponse> call, Response<BookResponse> response) {
                String message = response.body().getMessage();

                if (message.equals("success")){
                    Toast.makeText(getContext(), "Story is uploaded", Toast.LENGTH_SHORT).show();
                    moveToHome();
                }
                else {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BookResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void moveToHome() {
        Fragment fragment = HomeFragment.newInstance();

        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
