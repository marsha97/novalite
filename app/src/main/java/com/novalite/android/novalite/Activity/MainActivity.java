package com.novalite.android.novalite.Activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;

import com.novalite.android.novalite.Activity.SingleFragment;
import com.novalite.android.novalite.Fragment.CreateStoryFragment;
import com.novalite.android.novalite.Fragment.FavouriteFragment;
import com.novalite.android.novalite.Fragment.HomeFragment;
import com.novalite.android.novalite.Fragment.ProfileFragment;
import com.novalite.android.novalite.Fragment.UpdateFragment;
import com.novalite.android.novalite.R;

/**
 * Created by kali on 7/7/17.
 */

public class MainActivity extends SingleFragment implements View.OnClickListener{
    private Button homeBtn, favBtn, updateBtn, profileBtn, createBtn;

    @Override
    protected Fragment createFragment() {
        Fragment homeFragment = HomeFragment.newInstance();
        return  homeFragment;
    }

    @Override
    protected void initializeButton() {
        homeBtn = (Button) findViewById(R.id.homeBtn);
        updateBtn = (Button) findViewById(R.id.updateBtn);
        profileBtn = (Button) findViewById(R.id.profileBtn);
        createBtn = (Button) findViewById(R.id.createBtn);

        homeBtn.setOnClickListener(MainActivity.this);
        updateBtn.setOnClickListener(MainActivity.this);
        profileBtn.setOnClickListener(MainActivity.this);
        createBtn.setOnClickListener(MainActivity.this);
    }

    @Override
    public void onClick(View view) {
        Fragment fragment = HomeFragment.newInstance();
        String title = "Novalite";

        if (view == homeBtn){
            title = "Novalite";
            fragment = HomeFragment.newInstance();
        }
        else if (view == updateBtn) {
            title = "Update";
            fragment = UpdateFragment.newInstance();
        }
        else if (view == createBtn){
            title = "Create New Story";
            fragment = CreateStoryFragment.newInstance();
        }
        else if (view == profileBtn){
            title = getUsername() + "'s Page";
            fragment = ProfileFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString("username", getUsername());
            fragment.setArguments(bundle);
        }
        setTitle(title);
        super.replaceFragments(fragment);
    }

    private String getUsername(){
        SharedPreferences sharedPreferences = getSharedPreferences("UserInfo",0);
        return sharedPreferences.getString("username", "anon");
    }
}
