package com.novalite.android.novalite.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by kali on 7/10/17.
 */

public class BookResponse {

    @SerializedName("message")
    String message;

    @SerializedName("status")
    String status;

    @SerializedName("id")
    int id;

    @SerializedName("result")
    ArrayList<Book> mBooks;

    @SerializedName("resultChapter")
    ArrayList<Chapter> mChapters;

    public ArrayList<Chapter> getChapters(){
        return mChapters;
    }

    public ArrayList<Book> getBooks(){
        return mBooks;
    }

    public String getStatus(){
        return status;
    }

    public int getId(){
        return id;
    }

    public String getMessage(){
        return message;
    }
}
