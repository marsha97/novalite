package com.novalite.android.novalite.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.novalite.android.novalite.Client.novaliteClient;
import com.novalite.android.novalite.Interface.NovaliteAPI;
import com.novalite.android.novalite.Model.UserResponse;
import com.novalite.android.novalite.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText usernameEdit, passwordEdit;
    private String username, password;
    private Button loginBtn, regBtn;
    private NovaliteAPI mAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameEdit = (EditText) findViewById(R.id.editUser);
        passwordEdit = (EditText) findViewById(R.id.editPass);
        loginBtn = (Button) findViewById(R.id.btnLogin);
        regBtn = (Button) findViewById(R.id.regButton);

        loginBtn.setOnClickListener(this);
        regBtn.setOnClickListener(this);
        novaliteClient client = new novaliteClient();
        mAPI = client.getRetrofitClient().create(NovaliteAPI.class);

    }

    @Override
    public void onClick(View view) {
        if (view == loginBtn){
            username = usernameEdit.getText().toString();
            password = passwordEdit.getText().toString();
            Call<UserResponse> call = mAPI.loginUser(username, password);

            call.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    if (response.isSuccessful()){
                        String message = response.body().getMessageFromServer();

                        if (message.equals("success")){
                            setSharedPreference();
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                        else {
                            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(LoginActivity.this, response.code() + " " + response.message(), Toast.LENGTH_LONG).show();
                    }
//                    Toast.makeText(LoginActivity.this, new Gson().toJson(response.body()).toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    Log.i("LOGIN_LOG", "t= " + t.getMessage());

                }
            });

        }
        else if (view == regBtn){
            Intent intent = new Intent(LoginActivity.this, RegisterActiviity.class);
            startActivity(intent);
        }
    }

    private void setSharedPreference() {
        SharedPreferences preference = getSharedPreferences("UserInfo",0);
        SharedPreferences.Editor editor = preference.edit();

        editor.putString("user_status", "loggedIn");
        editor.putString("username", username);
        editor.commit();
    }
}
