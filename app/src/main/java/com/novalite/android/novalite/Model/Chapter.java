package com.novalite.android.novalite.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kali on 7/21/17.
 */

public class Chapter {
    @SerializedName("chapter")
    private int chapter;

    @SerializedName("chapterTitle")
    private String chapterTitle;

    @SerializedName("updateDate")
    private String update;

    @SerializedName("id")
    private int id;

    @SerializedName("bookId")
    private int bookId;

    @SerializedName("story")
    private String story;

    private int getBookId(){
        return bookId;
    }

    public int getChapter() {
        return chapter;
    }

    public String getChapterTitle() {
        return chapterTitle;
    }

    public String getUpdate() {
        return update;
    }

    public int getChapterId() {
        return id;
    }

    public String getStory()
    {
        return story;
    }

}
