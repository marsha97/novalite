package com.novalite.android.novalite.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kali on 7/6/17.
 */

public class UserResponse {
    @SerializedName("message")
    @Expose
    String message;

    public String getMessageFromServer(){
        return message;
    }
}
