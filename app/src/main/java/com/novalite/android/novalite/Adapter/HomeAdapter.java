package com.novalite.android.novalite.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.novalite.android.novalite.Model.Book;
import com.novalite.android.novalite.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by kali on 7/20/17.
 */

public class HomeAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Book> mBookArrayList;

    public HomeAdapter(Context mContext, ArrayList<Book> mBookArrayList){
        this.mContext = mContext;
        this.mBookArrayList = mBookArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null)
        {
            gridView = new View(mContext);

            gridView = inflater.inflate(R.layout.grid_content, null);

            ImageView cover = (ImageView) gridView.findViewById(R.id.coverImage);
            TextView judulText = (TextView) gridView.findViewById(R.id.titleText);
            TextView authorText = (TextView) gridView.findViewById(R.id.authorText);

            Picasso.with(mContext)
                    .load(mBookArrayList.get(position).getCover())
                    .placeholder(R.drawable.novalite)
                    .resize(150,200)
                    .into(cover);
            judulText.setText(mBookArrayList.get(position).getTitle());
            authorText.setText(mBookArrayList.get(position).getAuthor());
        }
        else {
            gridView = (View) convertView;
        }
        return gridView;
    }

    @Override
    public int getCount() {
        return mBookArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}
